import express, { Request, Response, Router } from "express";
import { AppDataSource } from "../../data-source";
import { User } from "../entity/User";
export class UsersController {
  private router: Router;

  constructor() {
    this.router = express.Router();
    this.router.get("/byid/:id", this.getUserById);
    // this.router.get("/byid", this.getUserById);
    this.router.get("/", this.getUsers);
  }

  public getRouter = () => {
    return this.router;
  };

  private getUsers = async (req: Request, res: Response) => {
    try {
      const userRepository = AppDataSource.getRepository(User);
      const allUsers = await userRepository.find();
      return res.status(200).json(allUsers);
    } catch (error) {
      return res.status(400).send(error);
    }
  };

  private getUserById = async (req: Request, res: Response) => {
    try {
      const id = req.params["id"];
      const userRepository = AppDataSource.getRepository(User);
      const user = await userRepository.findOneBy({ id: Number(id) });
      return res.status(200).json({ user });
    } catch (error) {
      return res.status(400).send(error);
    }
  };
}
